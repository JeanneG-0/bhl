<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Shelf Controller
 *
 * @property \App\Model\Table\ShelfTable $Shelf
 * @method \App\Model\Entity\Shelf[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ShelfController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Area']
        ];

        $this->set('shelf', $this->paginate($this->Shelf));
    }

    /**
     * View method
     *
     * @param string|null $id Shelf id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $shelf = $this->Shelf->get($id, [
            'contain' => ['Area'],
        ]);

        $this->set(compact('shelf'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $shelf = $this->Shelf->newEmptyEntity();

        $areaTable = $this->getTableLocator()->get('Area');
        $areaContent = $areaTable->find('all');
        $areas = $areaContent->toArray();
        $this->set(compact('areas'));

        if ($this->request->is('post')) {
            $shelf = $this->Shelf->patchEntity($shelf, $this->request->getData());
            $shelf['id_area'] = $this->request->getData()['id_area'];
            if ($this->Shelf->save($shelf)) {
                $this->Flash->success(__('L\'étagère a été enregistré.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('L\'étagère n\'a pas pu être enregistré. Veuillez réessayer.'));
        }
        $this->set(compact('shelf'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Shelf id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $shelf = $this->Shelf->get($id, [
            'contain' => [],
        ]);

        $areaTable = $this->getTableLocator()->get('Area');
        $areaContent = $areaTable->find('all');
        $areas = $areaContent->toArray();
        $this->set(compact('areas'));

        if ($this->request->is(['patch', 'post', 'put'])) {
            $shelf = $this->Shelf->patchEntity($shelf, $this->request->getData());
            $shelf['id_area'] = $this->request->getData()['area'];
            if ($this->Shelf->save($shelf)) {
                $this->Flash->success(__('L\'étagère a été enregistré.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('L\'étagère n\'a pas pu être enregistré. Veuillez réessayer.'));
        }
        $this->set(compact('shelf'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Shelf id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $shelf = $this->Shelf->get($id);
        if ($this->Shelf->delete($shelf)) {
            $this->Flash->success(__('L\'étagère a été supprimé.'));
        } else {
            $this->Flash->error(__('L\'étagère n\'a pas pu être supprimé. Veuillez réessayer.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
