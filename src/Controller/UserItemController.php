<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * UserItem Controller
 *
 * @property \App\Model\Table\UserItemTable $UserItem
 * @method \App\Model\Entity\UserItem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserItemController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $userItem = $this->paginate($this->UserItem);

        $this->set(compact('userItem'));
    }

    /**
     * View method
     *
     * @param string|null $id User Item id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userItem = $this->UserItem->get($id, [
            'contain' => ['Users', 'Item'],
        ]);

        $this->set(compact('userItem'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userItem = $this->UserItem->newEmptyEntity();
        if ($this->request->is('post')) {
            $userItem = $this->UserItem->patchEntity($userItem, $this->request->getData());
            if ($this->UserItem->save($userItem)) {
                $this->Flash->success(__('Le matériel de l\'utilisateur a été enregistré.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Le matériel de l\'utilisateur n\'a pas pu être enregistré. Veuillez réessayer.'));
        }
        $this->set(compact('userItem'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Item id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userItem = $this->UserItem->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userItem = $this->UserItem->patchEntity($userItem, $this->request->getData());
            if ($this->UserItem->save($userItem)) {
                $this->Flash->success(__('Le matériel de l\'utilisateur a été enregistré.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Le matériel de l\'utilisateur n\'a pas pu être enregistré. Veuillez réessayer.'));
        }
        $this->set(compact('userItem'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Item id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userItem = $this->UserItem->get($id);
        if ($this->UserItem->delete($userItem)) {
            $this->Flash->success(__('Le matériel de l\'utilisateur a été supprimé.'));
        } else {
            $this->Flash->error(__('Le matériel de l\'utilisateur n\'a pas pu être supprimé. Veuillez réessayer.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
