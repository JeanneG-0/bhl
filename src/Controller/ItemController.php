<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Item Controller
 *
 * @property \App\Model\Table\ItemTable $Item
 * @method \App\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemController extends AppController
{
	/**
	 * Index method
	 *
	 * @return \Cake\Http\Response|null|void Renders view
	 */
	public function index()
	{
        $this->paginate = [
            'contain' => ['Category', 'Users']
        ];

		$this->set('item', $this->paginate($this->Item));
	}

	/**
	 * View method
	 *
	 * @param string|null $id Item id.
	 * @return \Cake\Http\Response|null|void Renders view
	 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$item = $this->Item->get($id, [
			'contain' => ['Category', 'Users']
		]);

        $this->set(compact('item'));
	}

	/**
	 * Add method
	 *
	 * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$item = $this->Item->newEmptyEntity();

		$userTable = $this->getTableLocator()->get('Users');
        $userContent = $userTable->find('all');
        $users = $userContent->toArray();
        $this->set(compact('users'));

		$categoryTable = $this->getTableLocator()->get('Category');
        $categoryContent = $categoryTable->find('all');
        $categories = $categoryContent->toArray();
        $this->set(compact('categories'));

		if ($this->request->is('post')) {
			$file = $this->request->getData('photo');
			if ($file->getClientFilename() == ''){
                $this->Flash->error(__('Vous devez ajouter une image.'));
			}
			else {
				$targetPath = WWW_ROOT."img".DS."Items".DS.$file->getClientFilename();
				$file->moveTo($targetPath);
				$newItem = $this->request->getData();
				$newItem['photo'] =  "Items".DS.$file->getClientFilename();
				$item = $this->Item->patchEntity($item, $newItem);
				if ($this->Item->save($item)) {
					$this->Flash->success(__('Le matériel a été enregistré.'));

					return $this->redirect(['action' => 'index']);
				}
				$this->Flash->error(__('Le matériel n\'a pas pu être enregistré. Veuillez réessayer.'));
			}
		}
		$this->set(compact('item'));
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Item id.
	 * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$item = $this->Item->get($id, [
			'contain' => [],
		]);

		$userTable = $this->getTableLocator()->get('Users');
        $userContent = $userTable->find('all');
        $users = $userContent->toArray();
        $this->set(compact('users'));

		$categoryTable = $this->getTableLocator()->get('Category');
        $categoryContent = $categoryTable->find('all');
        $categories = $categoryContent->toArray();
        $this->set(compact('categories'));
		
		if ($this->request->is(['patch', 'post', 'put'])) {
			$file = $this->request->getData('photo');
			if ($file->getClientFilename() == ''){
                $newItem = $this->request->getData();
                $newItem['photo'] =  $item['photo'];
				$item = $this->Item->patchEntity($item, $newItem);
				if ($this->Item->save($item)) {
					$this->Flash->success(__('Le matériel a été enregistré.'));

					return $this->redirect(['action' => 'index']);
				}
				$this->Flash->error(__('Le matériel n\'a pas pu être enregistré. Veuillez réessayer.'));
			} else {
				$targetPath = WWW_ROOT."img".DS."Items".DS.$file->getClientFilename();
				$file->moveTo($targetPath);
				$newItem = $this->request->getData();
				$newItem['photo'] =  "Items".DS.$file->getClientFilename();
				$item = $this->Item->patchEntity($item, $newItem);
				if ($this->Item->save($item)) {
					$this->Flash->success(__('Le matériel a été enregistré.'));
					return $this->redirect(['action' => 'index']);
				}
				$this->Flash->error(__('Le matériel n\'a pas pu être enregistré. Veuillez réessayer.'));
			}
		}
		$this->set(compact('item'));
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Item id.
	 * @return \Cake\Http\Response|null|void Redirects to index.
	 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$item = $this->Item->get($id);
		if ($this->Item->delete($item)) {
			$this->Flash->success(__('Le matériel a été supprimé.'));
		} else {
			$this->Flash->error(__('Le matériel n\'a pas pu être supprimé. Veuillez réessayer.'));
		}

		return $this->redirect(['action' => 'index']);
	}

    /**
     * Loan method
     *
     * @param string|null $id Item id.
     * @return \Cake\Http\Response|null|void Redirects on successful loan, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function loan($id = null)
    {
        $item = $this->Item->get($id, [
            'contain' => [],
        ]);
        $uid = $_SESSION['Auth']['id'];
        $item['id_address'] = $uid;

        $item = $this->Item->patchEntity($item, $this->request->getData());
        if ($this->Item->save($item)) {
            $this->Flash->success(__('Le matériel a été emprunté.'));
            return $this->redirect(['action' => 'list']);
        }
        $this->Flash->error(__('Le matériel n\'a pas pu être emprunté. Veuillez réessayer.'));

        $this->set(compact('item'));
    }

	/**
     * Return method
     *
     * @param string|null $id Item id.
     * @return \Cake\Http\Response|null|void Redirects on successful loan, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function return($id = null)
    {
        $item = $this->Item->get($id, [
            'contain' => [],
        ]);

        $item['id_address'] = $item['id_owner'];

        $item = $this->Item->patchEntity($item, $this->request->getData());
        if ($this->Item->save($item)) {
            $this->Flash->success(__('Le matériel a été rendu.'));
            return $this->redirect(['action' => 'list']);
        }
        $this->Flash->error(__('Le matériel n\'a pas pu être rendu. Veuillez réessayer.'));

        $this->set(compact('item'));
    }

	/**
     * List method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function list()
    {
		$uid = $_SESSION['Auth']['id'];
		$this->paginate = [
            'contain' => ['Category', 'Users']

        ];
		$this->set('item', $this->paginate($this->Item));

        $query = $this->Item->find('all')
			->where(['Item.id_address' => $uid]);
		foreach ($query as $row) {
		}
		$results = $query->all();
		$data = $results->toArray();

		$this->set(compact('data'));
    }
}
