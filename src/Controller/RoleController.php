<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Role Controller
 *
 * @property \App\Model\Table\RoleTable $Role
 * @method \App\Model\Entity\Role[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RoleController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $role = $this->paginate($this->Role);

        $this->set(compact('role'));
    }

    /**
     * View method
     *
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $role = $this->Role->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('role'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $role = $this->Role->newEmptyEntity();
        if ($this->request->is('post')) {
            $role = $this->Role->patchEntity($role, $this->request->getData());
            if ($this->Role->save($role)) {
                $this->Flash->success(__('Le rôle a été enregistré.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Le rôle n\'a pas pu être enregistré. Veuillez réessayer.'));
        }
        $this->set(compact('role'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $role = $this->Role->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $role = $this->Role->patchEntity($role, $this->request->getData());
            if ($this->Role->save($role)) {
                $this->Flash->success(__('Le rôle a été enregistré.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Le rôle n\'a pas pu être enregistré. Veuillez réessayer.'));
        }
        $this->set(compact('role'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $role = $this->Role->get($id);
        if ($this->Role->delete($role)) {
            $this->Flash->success(__('Le rôle a été supprimé'));
        } else {
            $this->Flash->error(__('Le rôle n\'a pas pu être supprimé. Veuillez réessayer.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
