<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
{
    parent::beforeFilter($event);
    // Configurez l'action de connexion pour ne pas exiger d'authentification,
    // �vitant ainsi le probl�me de la boucle de redirection infinie
    $this->Authentication->addUnauthenticatedActions(['login']);
}

public function login()
{
    $this->request->allowMethod(['get', 'post']);
    $result = $this->Authentication->getResult();
    // ind�pendamment de POST ou GET, rediriger si l'utilisateur est connect�
    if ($result->isValid()) {
        // rediriger vers /articles apr�s la connexion r�ussie
        $redirect = $this->request->getQuery('redirect', [
            'controller' => 'Pages',
            'action' => 'index',
        ]);

        return $this->redirect($redirect);
    }
    // afficher une erreur si l'utilisateur a soumis le formulaire
    // et que l'authentification a �chou�
    if ($this->request->is('post') && !$result->isValid()) {
        $this->Flash->error(__('Votre identifiant ou votre mot de passe est incorrect.'));
    }
}

public function logout()
{
    $result = $this->Authentication->getResult();
    // ind�pendamment de POST ou GET, rediriger si l'utilisateur est connect�
    if ($result->isValid()) {
        $this->Authentication->logout();
        return $this->redirect(['controller' => 'Users', 'action' => 'login']);
    }
}
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Type', 'Role']
        ];

        $this->set('users', $this->paginate($this->Users));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Type', 'Role'],
        ]);

        $this->set(compact('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        $typeTable = $this->getTableLocator()->get('Type');
        $typeContent = $typeTable->find('all');
        $types = $typeContent->toArray();
        $this->set(compact('types'));

        $roleTable = $this->getTableLocator()->get('Role');
        $roleContent = $roleTable->find('all');
        $roles = $roleContent->toArray();
        $this->set(compact('roles'));


        if ($this->request->is('post')) {
            $file = $this->request->getData('picture');
            if ($file->getClientFilename() == ''){
                $this->Flash->error(__('Vous devez ajouter une image.'));
            }
                else {
                $targetPath = WWW_ROOT."img".DS."Users".DS.$file->getClientFilename();
                $file->moveTo($targetPath);
                $newUser = $this->request->getData();
                $newUser['picture'] =  "Users".DS.$file->getClientFilename();
                $user = $this->Users->patchEntity($user, $newUser);
                $user['type']=$newUser['type'];
                $user['role']=$newUser['role'];
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('L\'utilisateur a été enregistré.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('L\'utilisateur n\'a pas pu être enregistré. Veuillez réessayer.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        
        $typeTable = $this->getTableLocator()->get('Type');
        $typeContent = $typeTable->find('all');
        $types = $typeContent->toArray();
        $this->set(compact('types'));

        $roleTable = $this->getTableLocator()->get('Role');
        $roleContent = $roleTable->find('all');
        $roles = $roleContent->toArray();
        $this->set(compact('roles'));
        if ($this->request->is(['patch', 'post', 'put'])) {
            $file = $this->request->getData('picture');
            if ($file->getClientFilename() == ''){
                $newUser = $this->request->getData();
                $newUser['picture'] =  $user['picture'];
                $user = $this->Users->patchEntity($user, $newUser);
                $user['type']=$newUser['type'];
                $user['role']=$newUser['role'];
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('L\'utilisateur a été enregistré.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('L\'utilisateur n\'a pas pu être enregistré. Veuillez réessayer.'));
            } else {
                $targetPath = WWW_ROOT."img".DS."Users".DS.$file->getClientFilename();
                $file->moveTo($targetPath);
                $newUser = $this->request->getData();
                $newUser['picture'] =  "Users".DS.$file->getClientFilename();
                $user = $this->Users->patchEntity($user, $newUser);
                $user['type']=$newUser['type'];
                $user['role']=$newUser['role'];
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('L\'utilisateur a été enregistré.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('L\'utilisateur n\'a pas pu être enregistré. Veuillez réessayer.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('L\'utilisateur a été supprimé.'));
        } else {
            $this->Flash->error(__('L\'utilisateur n\'a pas pu être supprimé. Veuillez réessayer.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
