<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity
 *
 * @property int $id
 * @property string $photo
 * @property string $name
 * @property int $id_category
 * @property int $id_owner
 * @property int $id_address
 * @property string $description
 * @property string $qr_code
 */
class Item extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'photo' => true,
        'name' => true,
        'id_category' => true,
        'id_owner' => true,
        'id_address' => true,
        'description' => true,
        'qr_code' => true,
    ];
}
