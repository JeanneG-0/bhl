<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserItem Model
 *
 * @method \App\Model\Entity\UserItem newEmptyEntity()
 * @method \App\Model\Entity\UserItem newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\UserItem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserItem get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserItem findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\UserItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserItem[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserItem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserItem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserItem[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\UserItem[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\UserItem[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\UserItem[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class UserItemTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('user_item');

        $this->belongsTo('Users')->setForeignKey('id_user');
        $this->belongsTo('Item')->setForeignKey('id_item');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id_user')
            ->requirePresence('id_user', 'create')
            ->notEmptyString('id_user');

        $validator
            ->integer('id_item')
            ->requirePresence('id_item', 'create')
            ->notEmptyString('id_item');

        return $validator;
    }
}
