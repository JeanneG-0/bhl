<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Shelf Model
 *
 * @method \App\Model\Entity\Shelf newEmptyEntity()
 * @method \App\Model\Entity\Shelf newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Shelf[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Shelf get($primaryKey, $options = [])
 * @method \App\Model\Entity\Shelf findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Shelf patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Shelf[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Shelf|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Shelf saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Shelf[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Shelf[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Shelf[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Shelf[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ShelfTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('shelf');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Area')->setForeignKey('id_area');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->integer('id_area')
            ->requirePresence('id_area', 'create')
            ->notEmptyString('id_area');

        return $validator;
    }
}
