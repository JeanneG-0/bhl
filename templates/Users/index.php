<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="users index content">
    <?= $this->Html->link(__('Ajouter un utilisateur'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <a href="/role" class="button float-right" style="margin-right: 3rem">Gérer les rôles </a>
    <h3><?= __('Liste des utilisateurs') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th class="actions">_____________</th>
                    <th class="actions">Prénom</th>
                    <th class="actions">Nom</th>
                    <th class="actions">Adresse</th>
                    <th class="actions">Code postal</th>
                    <th class="actions">Ville</th>
                    <th class="actions">Type</th>
                    <th class="actions">Rôle</th>
                    <th class="actions">Adresse email</th>
                    <th class="actions">N° de tel</th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= $this->Html->image($user->picture) ?></td>
                    <td><?= h($user->first_name) ?></td>
                    <td><?= h($user->last_name) ?></td>
                    <td><?= h($user->address) ?></td>
                    <td><?= h($user->zip_code) ?></td>
                    <td><?= h($user->city) ?></td>
                    <td><?= h($user->type->name) ?></td>
                    <td><?= h($user->role->name) ?></td>
                    <td><?= h($user->email) ?></td>
                    <td><?= h($user->phone) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Afficher'), ['action' => 'view', $user->id]) ?>
                        <?= $this->Html->link(__('Modifier'), ['action' => 'edit', $user->id]) ?>
                        <?= $this->Form->postLink(__('Supprimer'), ['action' => 'delete', $user->id], ['confirm' => __('Êtes vous sûr de vouloir supprimer {0} {1}?', $user->first_name,$user->last_name)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('Précédent')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Suivant') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} sur {{pages}}, affichage de {{current}} enregistrements sur {{count}} au total')) ?></p>
    </div>
</div>
