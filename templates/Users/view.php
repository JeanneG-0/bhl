<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Modifier l\'utilisateur'), ['action' => 'edit', $user->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Supprimer l\'utilisateur'), ['action' => 'delete', $user->id], ['confirm' => __('Êtes vous sûr de vouloir supprimer {0} {1} ?', $user->first_name,$user->last_name), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Lister les utilisateurs'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Ajouter un utilisateur'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="users view content">
            <h3><?= h($user->first_name)?> <?=h($user->last_name) ?></h3>
            <table>
                <tr>
                    <th><?= __('') ?></th>
                    <td><?= $this->Html->image($user->picture, ['class' => 'picture-view']) ?></td>
                </tr>
                <tr>
                    <th><?= __('Prénom') ?></th>
                    <td><?= h($user->first_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Nom') ?></th>
                    <td><?= h($user->last_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Adresse') ?></th>
                    <td><?= h($user->address) ?></td>
                </tr>
                <tr>
                    <th><?= __('Code Postal') ?></th>
                    <td><?= h($user->zip_code) ?></td>
                </tr>
                <tr>
                    <th><?= __('Ville') ?></th>
                    <td><?= h($user->city) ?></td>
                </tr>
                <tr>
                    <th><?= __('Type') ?></th>
                    <td><?= h($user->type->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Rôle') ?></th>
                    <td><?= h($user->role->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Adresse email') ?></th>
                    <td><?= h($user->email) ?></td>
                </tr>
                <tr>
                    <th><?= __('Numéro de téléphone') ?></th>
                    <td><?= h($user->phone) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
