<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Supprimer'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Êtes vous sûr de vouloir supprimer {0} {1} ?', $user->first_name,$user->last_name), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('Lister les utilisateurs'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="formclass">
            <?= $this->Form->create($user, ['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('Modifier l\'utilisateur') ?></legend>
                <?php
                    echo $this->Form->control('picture', ['type' => 'file', 'label' => 'Photo']);
                    echo $this->Form->control('first_name', ['label' => 'Prénom']);
                    echo $this->Form->control('last_name', ['label' => 'Nom']);
                    echo $this->Form->control('address', ['label' => 'Adresse']);
                    echo $this->Form->control('zip_code', ['label' => 'Code postal']);
                    echo $this->Form->control('city', ['label' => 'Ville']);
                    ?>
                    <div class="input select">
                        <label for="type">Type</label>
                        <select name="type" id="type">
                        <?php
                        foreach ($types as $type): ?>
                            <option name=<?= $type->name ?> value=<?= $type->id ?>><?= $type->name ?></option>
                        <?php endforeach ?>
                        </select>          
                    </div>
                    
                    <div class="input select">
                        <label for="role">Role</label>
                        <select name="role" id="role">
                        <?php
                        foreach ($roles as $role): ?>
                            <option name=<?= $role->name ?> value=<?= $role->id ?>><?= $role->name ?></option>
                        <?php endforeach ?>
                        </select>          
                    </div>
                    <?php
                    echo $this->Form->control('email', ['label' => 'Adresse email']);
                    echo $this->Form->control('phone', ['label' => 'Numéro de téléphone']);
                    echo $this->Form->control('password', ['label' => 'Mot de passe']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Modifier')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
