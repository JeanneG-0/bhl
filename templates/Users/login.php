<div class="loginimg">
    <?= $this->Flash->render() ?>
    <img src="/img/logo-bhl.png" alt="Logo Bounty Hunter Legion" />
</div>
<div class="loginclass">
    <h3>Se connecter</h3>
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Veuillez s\'il vous plaît entrer votre nom d\'utilisateur et votre mot de passe') ?></legend>
        <?= $this->Form->control('email', ['required' => true, 'label' => 'Adresse email']) ?>
        <?= $this->Form->control('password', ['required' => true, 'label' => 'Mot de passe']) ?>
    </fieldset>
    <?= $this->Form->submit(__('Se connecter')); ?>
    <?= $this->Form->end() ?>

</div>
