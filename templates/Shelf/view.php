<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Shelf $shelf
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Modifier l\'étagère'), ['action' => 'edit', $shelf->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Supprimer l\'étagère'), ['action' => 'delete', $shelf->id], ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $shelf->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Lister les étagères'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Ajouter une étagère'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="shelf view content">
            <h3><?= h($shelf->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Libellé') ?></th>
                    <td><?= h($shelf->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Zone') ?></th>
                    <td><?= h($shelf->area->name) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
