<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Shelf $shelf
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Supprimer'),
                ['action' => 'delete', $shelf->id],
                ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $shelf->name), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('Lister les étagères'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="formclass">
            <?= $this->Form->create($shelf) ?>
            <fieldset>
                <legend><?= __('Modifier l\'étagère') ?></legend>
                <?php
                    echo $this->Form->control('name', ['label' => 'étagère']);
                    ?>
                    <div class="input select">
                        <label for="id_area">Zone</label>
                        <select name="id_area" id="id_area">
                        <?php
                        foreach ($areas as $area): ?>
                            <option name=<?= $area->name ?> value=<?= $area->id ?>><?= $area->name ?></option>
                        <?php endforeach ?>
                        </select>          
                    </div>
            </fieldset>
            <?= $this->Form->button(__('Modifier')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
