<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Shelf[]|\Cake\Collection\CollectionInterface $shelf
 */
?>
<div class="shelf index content">
    <?= $this->Html->link(__('Ajouter une étagère'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Liste des étagères') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th class="actions">Étagère</th>
                    <th class="actions">Zone</th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($shelf as $shelf): ?>
                <tr>
                    <td><?= h($shelf->name) ?></td>
                    <td><?= h($shelf->area->name) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Voir'), ['action' => 'view', $shelf->id]) ?>
                        <?= $this->Html->link(__('Modifier'), ['action' => 'edit', $shelf->id]) ?>
                        <?= $this->Form->postLink(__('Supprimer'), ['action' => 'delete', $shelf->id], ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $shelf->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('Précédent')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Suivant') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} sur {{pages}}, affichage de {{current}} enregistrements sur {{count}} au total')) ?></p>
    </div>
</div>
