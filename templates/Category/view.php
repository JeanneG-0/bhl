<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Modifier la catégorie'), ['action' => 'edit', $category->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Supprimer la catégorie'), ['action' => 'delete', $category->id], ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $category->name), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Lister les catégories'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Ajouter une catégorie'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="category view content">
            <h3><?= h($category->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Libellé') ?></th>
                    <td><?= h($category->name) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
