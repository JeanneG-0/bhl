<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Area[]|\Cake\Collection\CollectionInterface $area
 */
?>
<div class="area index content">
    <?= $this->Html->link(__('Ajouter une zone'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Liste des zones') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th class="actions">Libellé</th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($area as $area): ?>
                <tr>
                    <td><?= h($area->name) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Voir'), ['action' => 'view', $area->id]) ?>
                        <?= $this->Html->link(__('Modifier'), ['action' => 'edit', $area->id]) ?>
                        <?= $this->Form->postLink(__('Supprimer'), ['action' => 'delete', $area->id], ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $area->name)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('Précédent')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Suivant') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} sur {{pages}}, affichage de {{current}} enregistrements sur {{count}} au total')) ?></p>
    </div>
</div>
