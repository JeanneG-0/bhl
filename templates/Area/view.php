<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Area $area
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Modifier la zone'), ['action' => 'edit', $area->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Supprimer la zone'), ['action' => 'delete', $area->id], ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $area->name), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Lister les zones'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Ajouter une zone'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="area view content">
            <h3><?= h($area->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Libellé') ?></th>
                    <td><?= h($area->name) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
