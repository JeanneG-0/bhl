<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Area $area
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Supprimer'),
                ['action' => 'delete', $area->id],
                ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $area->name), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('Lister les zones'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="formclass">
            <?= $this->Form->create($area) ?>
            <fieldset>
                <legend><?= __('Modifier la zone') ?></legend>
                <?php
                    echo $this->Form->control('name');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Modifier')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
