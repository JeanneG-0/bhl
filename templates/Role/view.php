<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Role $role
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Modifier le rôle'), ['action' => 'edit', $role->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Supprimer le rôle'), ['action' => 'delete', $role->id], ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $role->name), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Lister les rôles'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Ajouter un rôle'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="role view content">
            <h3><?= h($role->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Libellé') ?></th>
                    <td><?= h($role->name) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
