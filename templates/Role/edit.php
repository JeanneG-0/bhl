<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Role $role
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Supprimer'),
                ['action' => 'delete', $role->id],
                ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $role->name), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('Lister les rôles'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="formclass">
            <?= $this->Form->create($role) ?>
            <fieldset>
                <legend><?= __('Modifier le rôle') ?></legend>
                <?php
                    echo $this->Form->control('name', ['label' => 'Rôle']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Modifier')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
