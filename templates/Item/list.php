<?php 
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item[]|\Cake\Collection\CollectionInterface $item
 */
?>
<div class="item index content">
    <?= $this->Html->link(__('Emprunter du matériel'), ['action' => 'index'], ['class' => 'button float-right']) ?>
    <h3><?= __('Liste du matériel emprunté') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('') ?></th>
                    <th><?= $this->Paginator->sort('Matériel') ?></th>
                    <th><?= $this->Paginator->sort('Catégorie') ?></th>
                    <th><?= $this->Paginator->sort('Propriétaire') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $data): ?>
                <tr>
                    <td><?= $this->Html->image($data->photo) ?></td>
                    <td><?= h($data->name) ?></td>
                    <td><?= h($item->first()->category->name) ?></td>
                    <td><?= h($item->first()->user->last_name) ?> <?= h($item->first()->user->first_name) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Rendre le matériel'), ['action' => 'return', $data->id], ['confirm' => __('Êtes vous sûr de vouloir rendre {0}?', $data->name)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('Précédent')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Suivant') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
    </div>
</div>
