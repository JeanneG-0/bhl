<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Supprimer'),
                ['action' => 'delete', $item->id],
                ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $item->name), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('Lister les matériels'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="formclass">
            <?= $this->Form->create($item, ['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('Modifier le matériel') ?></legend>
                <?php
                    echo $this->Form->control('photo', ['type' => 'file', 'label' => 'Photo']);
                    echo $this->Form->control('name', ['label' => 'Nom']);
                    ?>
                    <div class="input select">
                        <label for="id_category">Catégorie</label>
                        <select name="id_category" id="id_category">
                        <?php
                        foreach ($categories as $category): ?>
                            <option name=<?= $category->name ?> value=<?= $category->id ?>><?= $category->name ?></option>
                        <?php endforeach ?>
                        </select>          
                    </div>

                    <div class="input select">
                        <label for="id_owner">Propriétaire</label>
                        <select name="id_owner" id="id_owner">
                        <?php
                        foreach ($users as $user): ?>
                            <option name=<?= $user->first_name ?> value=<?= $user->id ?>><?= $user->first_name ?> <?= $user->last_name ?></option>
                        <?php endforeach ?>
                        </select>          
                    </div>

                    <div class="input select">
                        <label for="id_address">Adresse où le matériel est stocké</label>
                        <select name="id_address" id="id_address">
                        <?php
                        foreach ($users as $user): ?>
                            <option name=<?= $user->address ?> value=<?= $user->id ?>><?= $user->first_name ?> <?= $user->last_name ?>
                            <?= $user->address ?> <?= $user->zip_code?> <?= $user->city ?></option>
                        <?php endforeach ?>
                        </select>          
                    </div>
                    <?php
                    echo $this->Form->control('description', ['label' => 'Description']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Modifier')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
