<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Modifier le matériel'), ['action' => 'edit', $item->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Supprimer le matériel'), ['action' => 'delete', $item->id], ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $item->name), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Lister les matériels'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Ajouter un matériel'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Emprunter le matériel'), ['action' => 'loan', $item->id], ['confirm' => __('Êtes vous sûr de vouloir emprunter {0}?', $item->name),'class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="item view content">
            <h3><?= h($item->name) ?></h3>
            <table>
                <tr class="text">
                    <th><?= __('') ?></th>
                    <td><?= $this->Html->image($item->photo, ['class' => 'picture-view']) ?></td>
                </tr>
                <tr>
                    <th><?= __('Libellé') ?></th>
                    <td><?= h($item->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Catégorie du matériel') ?></th>
                    <td><?= h($item->category->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Propriétaire du matériel') ?></th>
                    <td> <?= h($item->user->last_name) ?> <?= h($item->user->first_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('L\'adresse où se situe le matériel') ?></th>
                    <td> <?=h($item->user->last_name)?> <?=h($item->user->first_name)?> <br/>
                    <?= h($item->user->address)?> <br/>
                    <?= h($item->user->zip_code)?> <?=h($item->user->city)?> </td>

                </tr>
            </table>
            <div class="text">
                <strong><?= __('Description') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($item->description)); ?>
                </blockquote>
            </div>
        </div>
    </div>
</div>
