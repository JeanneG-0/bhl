<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item[]|\Cake\Collection\CollectionInterface $item
 */
?>
<div class="item index content">
    <?= $this->Html->link(__('Ajouter un matériel'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Liste du matériel') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th class="actions">_____________</th>
                    <th class="actions">Libellé</th>
                    <th class="actions">Catégorie</th>
                    <th class="actions">Propriétaire</th>
                    <th class="actions">Adresse</th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($item as $item): ?>
                <tr>
                    <td><?= $this->Html->image($item->photo) ?></td>
                    <td><?= h($item->name) ?></td>
                    <td><?= h($item->category->name) ?></td>
                    <td><?= h($item->user->last_name) ?> <?= h($item->user->first_name) ?></td>
                    <td><?=h($item->user->last_name)?> <?=h($item->user->first_name)?> <br/>
                        <?= h($item->user->address)?> <br/>
                        <?= h($item->user->zip_code)?> <?=h($item->user->city)?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Afficher'), ['action' => 'view', $item->id]) ?>
                        <?= $this->Html->link(__('Emprunter'), ['action' => 'loan', $item->id], ['confirm' => __('Êtes vous sûr de vouloir emprunter {0}?', $item->name)]) ?>
                        <?= $this->Html->link(__('Modifier'), ['action' => 'edit', $item->id]) ?>
                        <?= $this->Form->postLink(__('Supprimer'), ['action' => 'delete', $item->id], ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}?', $item->name)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('Précédent')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Suivant') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} sur {{pages}}, affichage de {{current}} enregistrements sur {{count}} au total')) ?></p>
    </div>
</div>
