<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'BHL';
$this->loadHelper('Authentication.Identity');
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bounty Hunters Legion</title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <?= $this->Html->css(['normalize.min', 'milligram.min', 'cake']) ?>
    <?= $this->Html->script(['dropdown.js']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="top-nav">
        <div class="top-nav-title">
            <?php if ($this->Identity->isLoggedIn()){ ?>
                <img src="/img/logo-bhl.png" class="logo" alt="Logo Bounty Hunter Legion" />
        </div>
        <div class="dropdown">
                <a href="<?= $this->Url->build('pages/index') ?>">Accueil</a>
                <a href="#" onclick="drop()" class="drop" id="drop">Matériel</a>
                    <div id="dropthis" class="dropdown-content">
                        <a href="<?= $this->Url->build('item/') ?>">Gérer le matériel</a>
                        <a href="<?= $this->Url->build('category/') ?>">Gérer les catégories</a>
                        <a href="<?= $this->Url->build('item/list') ?>">Gérer mes emprunts</a>
                    </div>
                <a href="#" onclick="drop2()" class="dropagain" id="dropagain">Stockage</a>
                <div id="dropthat" class="dropdomn-content">
                    <a href="<?= $this->Url->build('area/') ?>">Gérer les zones</a>
                    <a href="<?= $this->Url->build('shelf/') ?>">Gérer les étagères</a>
                    <a href="<?= $this->Url->build('type/') ?>">Gérer les types de stockage</a>
                </div>
                <a href="<?= $this->Url->build('users/') ?>">Membres</a>
                <?= $this->Html->link("Se déconnecter", ['controller' => 'Users', 'action' => 'logout']) ?>
            <?php } ?>

        </div>
    </nav>
    <main class="main">
        <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <footer>
    </footer>
</body>
</html>
