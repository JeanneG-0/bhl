<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Type $type
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Supprimer'),
                ['action' => 'delete', $type->id],
                ['confirm' => __('Êtes vous sûr de vouloir supprimer {0} ?', $type->name), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('Lister les types'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="formclass">
            <?= $this->Form->create($type) ?>
            <fieldset>
                <legend><?= __('Modifier un type') ?></legend>
                <?php
                    echo $this->Form->control('name', ['label' => 'Type']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Modifier')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
