<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Type[]|\Cake\Collection\CollectionInterface $type
 */
?>
<div class="type index content">
    <?= $this->Html->link(__('Ajouter un type'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Liste des types') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th class="actions">Libellé</th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($type as $type): ?>
                <tr>
                    <td><?= h($type->name) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Afficher'), ['action' => 'view', $type->id]) ?>
                        <?= $this->Html->link(__('Modifier'), ['action' => 'edit', $type->id]) ?>
                        <?= $this->Form->postLink(__('Supprimer'), ['action' => 'delete', $type->id], ['confirm' => __('Êtes vous sûr de vouloir supprimer {0}', $type->name)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('Précédent')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Suivant') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} sur {{pages}}, affichage de {{current}} enregistrements sur {{count}} au total')) ?></p>
    </div>
</div>
