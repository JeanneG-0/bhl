<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ShelfTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ShelfTable Test Case
 */
class ShelfTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ShelfTable
     */
    protected $Shelf;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Shelf',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Shelf') ? [] : ['className' => ShelfTable::class];
        $this->Shelf = $this->getTableLocator()->get('Shelf', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Shelf);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
